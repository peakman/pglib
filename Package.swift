// swift-tools-version:4.2
import PackageDescription

let package = Package(
    name: "PGLib",
    products: [
        .library(name: "PGLib", targets: ["PGLib"]),
    ],
    targets: [
        .systemLibrary(
            name: "libpq"),
        .target(
            name: "PGLib",
            dependencies: ["libpq"]),
    ]
)
